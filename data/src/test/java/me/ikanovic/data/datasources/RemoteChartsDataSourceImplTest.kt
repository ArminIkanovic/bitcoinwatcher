package me.ikanovic.data.datasources

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import me.ikanovic.data.api.ChartsApi
import me.ikanovic.data.entities.ChartResponse
import me.ikanovic.domain.dashboard.entities.Chart
import org.junit.Before
import org.junit.Test


class RemoteChartsDataSourceImplTest {

    private val timeSpan = "3days"

    private val chartApi = mockk<ChartsApi>()
    lateinit var remoteChartsDataSourceImpl: RemoteChartsDataSourceImpl

    @Before
    fun setUp() {
        remoteChartsDataSourceImpl = RemoteChartsDataSourceImpl(chartApi)
    }

    @Test
    fun `When load chart with chartId and time span is called, then chart api load chart with same params is invoked`() {
        //given
        every { chartApi.loadChart(any(), any()) }.returns(Single.never())

        //when
        remoteChartsDataSourceImpl.loadChartData(Chart.MARKET_CAP, timeSpan)
            .test()
            .dispose()

        //then
        verify { chartApi.loadChart(Chart.MARKET_CAP, timeSpan) }
    }

    @Test
    fun `When load chart is called and chart api returns result, then same result is returned`() {
        //given
        val chart = mockk<ChartResponse>()
        every { chartApi.loadChart(any(), any()) }.returns(Single.just(chart))

        //when
        val result = remoteChartsDataSourceImpl.loadChartData(Chart.MARKET_CAP, timeSpan)
            .test()

        //then
        result
            .assertResult(chart)
            .assertNoErrors()
            .assertComplete()
            .dispose()
    }


}