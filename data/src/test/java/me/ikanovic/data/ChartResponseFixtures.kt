package me.ikanovic.data

import me.ikanovic.data.entities.ChartResponse
import me.ikanovic.data.entities.ChartValue


val validChartResponse = ChartResponse(
    status = "ok",
    name = "Chart name",
    unit = "day",
    description = "Valid chart response",
    period = "period",
    values = listOf(
        ChartValue(0, 0.0),
        ChartValue(0, 1.0)
    )
)

val emptyChartResponse = ChartResponse(status = "")

val errorChartResponse = ChartResponse(
    status = "not-found",
    error = "params are not found"
)