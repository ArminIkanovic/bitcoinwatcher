package me.ikanovic.data.repositories

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import me.ikanovic.data.datasources.ChartsDataSource
import me.ikanovic.data.errorChartResponse
import me.ikanovic.data.mapper.ChartMapper
import me.ikanovic.data.validChartResponse
import org.junit.Before
import org.junit.Test


class DashboardRepositoryImplTest {

    private val chartId = "chartId"
    private val timeSpan = "timeSpan"

    private val dataSource = mockk<ChartsDataSource>()
    private val mapper = ChartMapper()

    lateinit var repository: DashboardRepositoryImpl

    @Before
    fun setUp() {
        repository = DashboardRepositoryImpl(dataSource, mapper)
    }

    @Test
    fun `When load chart data is called with params, then same params are forwaded to data source`() {
        // given
        every { dataSource.loadChartData(any(), any()) }.returns(Single.never())

        // when
        repository.loadChartData(chartId, timeSpan)
            .test()
            .dispose()

        // then
        verify { dataSource.loadChartData(chartId, timeSpan) }
    }

    @Test
    fun `When data source returns valid chart response, then chart response is mapped and result is returned`() {
        // given
        every { dataSource.loadChartData(any(), any()) }.returns(Single.just(validChartResponse))

        // when
        val result = repository.loadChartData(chartId, timeSpan)
            .test()

        // then
        result
            .assertNoErrors()
            .assertResult(mapper.map(Pair(validChartResponse, chartId)))
            .dispose()
    }

    @Test
    fun `When data source returns non-ok status, then exception is thrown containing status and error text`() {
        // given
        every { dataSource.loadChartData(any(), any()) }.returns(Single.just(errorChartResponse))

        // when
        val result = repository.loadChartData(chartId, timeSpan).test()

        // then
        result
            .assertErrorMessage("${errorChartResponse.status} - ${errorChartResponse.error}")
            .dispose()
    }
}
