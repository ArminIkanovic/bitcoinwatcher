package me.ikanovic.data.mapper

import me.ikanovic.data.emptyChartResponse
import me.ikanovic.data.validChartResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class ChartMapperTest {

    private val chartMapper = ChartMapper()
    private val chartId = "chartID"

    @Test
    fun `When map method is called with chart response and it contains all values, then result is mapped correctly`() {
        // given
        val input = validChartResponse

        // when
        val result = chartMapper.map(Pair(input, chartId))

        // then
        assertEquals(chartId, result.chartId)
        assertEquals(input.name, result.name)
        assertEquals(input.values?.first()?.x, result.values.first().x)
        assertEquals(input.values?.first()?.y, result.values.first().y)
    }

    @Test
    fun `When map method is called with empty chart response, then empty chart should be returned`() {
        // given
        val input = emptyChartResponse

        // when
        val result = chartMapper.map(Pair(input, chartId))

        // then
        assertEquals(chartId, result.chartId)
        assertTrue(result.name.isEmpty())
        assertTrue(result.values.isEmpty())
    }
}