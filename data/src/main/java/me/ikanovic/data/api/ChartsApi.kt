package me.ikanovic.data.api

import com.google.gson.internal.GsonBuildConfig
import io.reactivex.Single
import me.ikanovic.data.entities.ChartResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ChartsApi {

    @GET("charts/{chartId}")
    fun loadChart(
        @Path("chartId") chartId: String,
        @Query("timespan") timeSpan: String = "3days",
        @Query("format") format: String = "json"
    ): Single<ChartResponse>


    companion object {

        fun build(apiUrl: String): ChartsApi {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)

            val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)

            return Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ChartsApi::class.java)
        }

    }

}