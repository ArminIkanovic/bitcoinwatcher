package me.ikanovic.data.entities

enum class ResponseStatus(val status: String) {
    OK("ok"),
    OTHER("other")
}
