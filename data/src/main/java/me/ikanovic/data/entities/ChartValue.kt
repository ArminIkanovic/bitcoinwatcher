package me.ikanovic.data.entities

data class ChartValue(
    val x: Long,
    val y: Double
)
