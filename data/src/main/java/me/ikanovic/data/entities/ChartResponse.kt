package me.ikanovic.data.entities

data class ChartResponse(
    val status: String,
    val error: String? = null,
    val name: String? = null,
    val unit: String? = null,
    val period: String? = null,
    val description: String? = null,
    val values: List<ChartValue?>? = null
) {
    val responseStatus: ResponseStatus
        get() = ResponseStatus.values().firstOrNull { it.status == status } ?: ResponseStatus.OTHER
}
