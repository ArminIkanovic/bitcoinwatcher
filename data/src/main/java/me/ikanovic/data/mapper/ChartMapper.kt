package me.ikanovic.data.mapper

import me.ikanovic.data.entities.ChartResponse
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.entities.Period
import me.ikanovic.domain.dashboard.entities.Value
import javax.inject.Inject

class ChartMapper @Inject constructor() {

    fun map(input: Pair<ChartResponse, String>): Chart {
        val (chartResponse, id) = input
        return Chart(
            chartId = id,
            name = chartResponse.name.orEmpty(),
            unit = chartResponse.unit.orEmpty(),
            period = Period.from(chartResponse.period.orEmpty()),
            description = chartResponse.description.orEmpty(),
            values = chartResponse.values?.map { Value(it?.x ?: 0, it?.y ?: 0.0) } ?: emptyList()
        )
    }
}
