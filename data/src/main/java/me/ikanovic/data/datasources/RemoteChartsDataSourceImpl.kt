package me.ikanovic.data.datasources

import io.reactivex.Single
import me.ikanovic.data.api.ChartsApi
import me.ikanovic.data.entities.ChartResponse
import javax.inject.Inject

class RemoteChartsDataSourceImpl @Inject constructor(
    private val chartsApi: ChartsApi
) : ChartsDataSource {

    override fun loadChartData(
        chartId: String,
        timeSpan: String,
    ): Single<ChartResponse> {
        return chartsApi.loadChart(chartId, timeSpan)
    }
}
