package me.ikanovic.data.datasources

import me.ikanovic.domain.alerts.entities.Alert

interface AlertsDataSource {

    suspend fun getAlert(id: Long): Alert?

    suspend fun getAllAlerts(): List<Alert>?

    suspend fun createAlert(alert: Alert): Alert

    suspend fun deleteAlert(alert: Alert)

    suspend fun updateAlert(alert: Alert)

}
