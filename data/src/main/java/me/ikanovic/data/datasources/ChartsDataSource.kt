package me.ikanovic.data.datasources

import io.reactivex.Single
import me.ikanovic.data.entities.ChartResponse

interface ChartsDataSource {
    fun loadChartData(
        chartId: String,
        timeSpan: String
    ): Single<ChartResponse>
}
