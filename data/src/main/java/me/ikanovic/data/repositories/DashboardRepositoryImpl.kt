package me.ikanovic.data.repositories

import io.reactivex.Single
import me.ikanovic.data.datasources.ChartsDataSource
import me.ikanovic.data.entities.ResponseStatus
import me.ikanovic.data.mapper.ChartMapper
import me.ikanovic.domain.dashboard.DashboardRepository
import me.ikanovic.domain.dashboard.entities.Chart
import javax.inject.Inject

class DashboardRepositoryImpl @Inject constructor(
    private val remoteDataSource: ChartsDataSource,
    private val mapper: ChartMapper
) : DashboardRepository {

    override fun loadChartData(
        chartId: String,
        timeSpan: String
    ): Single<Chart> {
        return remoteDataSource.loadChartData(chartId, timeSpan)
            .map {
                when (it.responseStatus) {
                    ResponseStatus.OK -> mapper.map(Pair(it, chartId))
                    ResponseStatus.OTHER -> throw Exception("${it.status} - ${it.error ?: "unknown"}")
                }
            }
    }
}
