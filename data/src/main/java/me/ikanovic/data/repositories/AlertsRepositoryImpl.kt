package me.ikanovic.data.repositories

import me.ikanovic.data.datasources.AlertsDataSource
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.AlertsRepository
import javax.inject.Inject

class AlertsRepositoryImpl @Inject constructor(
    private val dataSource: AlertsDataSource
) : AlertsRepository {

    override suspend fun loadAlert(id: Long): Alert? {
        return dataSource.getAlert(id)
    }

    override suspend fun loadAll(): List<Alert> {
        return dataSource.getAllAlerts() ?: emptyList()
    }

    override suspend fun removeAlert(alert: Alert) {
        dataSource.deleteAlert(alert)
    }

    override suspend fun updateAlert(alert: Alert) {
        dataSource.updateAlert(alert)
    }

    override suspend fun saveAlert(alert: Alert) {
        dataSource.createAlert(alert)
    }
}