package me.ikanovic.bitcoinwatcher.alerts.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "room_alerts")
data class RoomAlert(
    @PrimaryKey(autoGenerate = true) val uid: Long,
    @ColumnInfo(name = "value") val value: Double,
    @ColumnInfo(name = "start_value") val startValue: Double,
    @ColumnInfo(name = "is_triggered") val isTriggered: Boolean
)