package me.ikanovic.bitcoinwatcher.alerts

import me.ikanovic.bitcoinwatcher.alerts.database.RoomAlert
import me.ikanovic.domain.alerts.entities.Alert
import javax.inject.Inject

class AlertRoomMapper @Inject constructor() {

    fun toRoomAlert(alert: Alert): RoomAlert {
        return RoomAlert(
            uid = alert.id,
            value = alert.value,
            startValue = alert.startValue,
            isTriggered = alert.isTriggered
        )
    }

    fun toAlert(roomAlert: RoomAlert): Alert {
        return Alert(
            id =  roomAlert.uid,
            value =  roomAlert.value,
            startValue =  roomAlert.startValue,
            isTriggered = roomAlert.isTriggered
        )
    }
}