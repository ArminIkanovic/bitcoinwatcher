package me.ikanovic.bitcoinwatcher.alerts.database

import androidx.room.*

@Dao
interface AlertsDao {

    @Query("SELECT * FROM room_alerts")
    suspend fun loadAll():List<RoomAlert>?

    @Query("SELECT * FROM room_alerts WHERE uid == :uid LIMIT 1")
    suspend fun findAlertsByUid(uid: Long): List<RoomAlert>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun createAlert(alert: RoomAlert): Long

    @Delete
    suspend fun deleteAlert(alert: RoomAlert)

}