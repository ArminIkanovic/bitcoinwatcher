package me.ikanovic.bitcoinwatcher.alerts.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [RoomAlert::class], version = 1, exportSchema = false)
abstract class AlertsDatabase : RoomDatabase() {

    abstract fun alertsDao(): AlertsDao

    companion object {

        fun getInstance(context: Context): AlertsDatabase {
            synchronized(this) {
                return Room.databaseBuilder(
                    context.applicationContext,
                    AlertsDatabase::class.java,
                    "alerts_database"
                ).fallbackToDestructiveMigration()
                    .build()

            }
        }
    }
}