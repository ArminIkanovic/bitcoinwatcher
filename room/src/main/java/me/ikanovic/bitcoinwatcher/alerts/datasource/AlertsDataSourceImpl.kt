package me.ikanovic.bitcoinwatcher.alerts.datasource

import me.ikanovic.bitcoinwatcher.alerts.AlertRoomMapper
import me.ikanovic.bitcoinwatcher.alerts.database.AlertsDao
import me.ikanovic.data.datasources.AlertsDataSource
import me.ikanovic.domain.alerts.entities.Alert
import javax.inject.Inject

class AlertsDataSourceImpl @Inject constructor(
    private val alertRoomMapper: AlertRoomMapper,
    private val alertsDao: AlertsDao
) : AlertsDataSource {

    override suspend fun getAlert(id: Long): Alert? {
        return alertsDao.findAlertsByUid(id)?.firstOrNull()?.let { alertRoomMapper.toAlert(it) }
    }

    override suspend fun getAllAlerts(): List<Alert>? {
        return alertsDao.loadAll()?.map { alertRoomMapper.toAlert(it) }
    }

    override suspend fun createAlert(alert: Alert): Alert {
        val id = alertsDao.createAlert(alertRoomMapper.toRoomAlert(alert))
        return alert.copy(id = id)
    }

    override suspend fun deleteAlert(alert: Alert) {
        alertsDao.deleteAlert(alertRoomMapper.toRoomAlert(alert))
    }

    override suspend fun updateAlert(alert: Alert) {
        alertsDao.createAlert(alertRoomMapper.toRoomAlert(alert))
    }
}
