package me.ikanovic.bitcoinwatcher.alerts

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import me.ikanovic.data.datasources.AlertsDataSource
import me.ikanovic.domain.alerts.entities.Alert
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class AlertsRepositoryImplTest {

    private val dataSource: AlertsDataSource = FakeAlertsDataSource()
    private lateinit var repositoryImpl: me.ikanovic.data.repositories.AlertsRepositoryImpl

    private val mainThreadSurrogate = newSingleThreadContext("main thread")


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        repositoryImpl = me.ikanovic.data.repositories.AlertsRepositoryImpl(dataSource)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `When no alerts are added, then load all returns empty list`() = runBlockingTest {
        // given && when
        val result = repositoryImpl.loadAll()

        // then
        assertEquals(0, result.size)
    }

    @Test
    fun `When alert was added and alert is loaded with id, then correct alert is returned`() =
        runBlockingTest {
            // given
            val newAlert = Alert(
                21L,
                100.0,
                90.0,
                false
            )
            repositoryImpl.saveAlert(newAlert)

            // when
            val result = repositoryImpl.loadAlert(newAlert.id)

            // then
            assertEquals(newAlert, result)
        }

    @Test
    fun `When alerts are added and load all is called, then all alerts are returned`() =
        runBlockingTest {
            // given
            val newAlert = Alert(
                21L,
                100.0,
                90.0,
                false
            )
            repositoryImpl.saveAlert(newAlert)
            repositoryImpl.saveAlert(newAlert.copy(id = 31L))
            repositoryImpl.saveAlert(newAlert.copy(id = 329L))

            // when
            val result = repositoryImpl.loadAll()

            // then
            assertEquals(3, result.size)
        }

    @Test
    fun `When alert is removed, then the removed id is not returned in load alert`() =
        runBlockingTest {
            // given
            val newAlert = Alert(
                21L,
                100.0,
                90.0,
                false
            )
            repositoryImpl.saveAlert(newAlert)

            // when
            repositoryImpl.removeAlert(newAlert)

            // then
            val result = repositoryImpl.loadAlert(newAlert.id)
            assertEquals(null, result)
        }

}