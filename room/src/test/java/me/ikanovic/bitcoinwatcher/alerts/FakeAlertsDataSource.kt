package me.ikanovic.bitcoinwatcher.alerts

import me.ikanovic.data.datasources.AlertsDataSource
import me.ikanovic.domain.alerts.entities.Alert

class FakeAlertsDataSource : AlertsDataSource {

    private val alerts = mutableListOf<Alert>()

    override suspend fun getAlert(id: Long): Alert? {
        return alerts.firstOrNull { it.id == id }
    }

    override suspend fun getAllAlerts(): List<Alert>? {
        return alerts
    }

    override suspend fun createAlert(alert: Alert): Alert {
        alerts.add(alert)
        return alert
    }

    override suspend fun deleteAlert(alert: Alert) {
        alerts.remove(alert)
    }

    override suspend fun updateAlert(alert: Alert) {
        alerts.replaceAll {
            if (it.id == alert.id) {
                alert
            } else {
                it
            }
        }
    }

}
