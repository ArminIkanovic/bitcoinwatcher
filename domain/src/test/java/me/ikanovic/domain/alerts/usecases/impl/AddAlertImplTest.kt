package me.ikanovic.domain.alerts.usecases.impl

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.dashboard.usecases.LoadCurrentPrice
import org.junit.After
import org.junit.Before
import org.junit.Test

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class AddAlertImplTest {

    private val mainThreadSurrogate = newSingleThreadContext("main")

    private val alertsRepository = mockk<AlertsRepository>(relaxed = true)
    private val currentPrice = mockk<LoadCurrentPrice>(relaxed = true)

    private lateinit var addAlertImpl: AddAlertImpl
    private val defaultCurrentPrice = 10.0

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        coEvery { currentPrice.currentPrice() } returns defaultCurrentPrice
        addAlertImpl = AddAlertImpl(alertsRepository, currentPrice)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `When add alert is called, then the param is forwarded to repository`() = runBlockingTest {
        // given
        val alert = Alert(value = 100.0)

        // when
        addAlertImpl.addAlert(alert)

        // then
        coEvery { alertsRepository.saveAlert(alert) }

    }

    @Test
    fun `When new alert is added and current price returns value, then the start value is set`() =
        runBlockingTest {
            // given
            val alert = Alert(value = 10.00)

            // when
            addAlertImpl.addAlert(alert)

            // then
            coVerify { currentPrice.currentPrice() }
            coVerify { alertsRepository.saveAlert(alert.copy(startValue = defaultCurrentPrice)) }
        }

}