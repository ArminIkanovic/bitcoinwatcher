package me.ikanovic.domain.alerts.usecases.impl

import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.AlertsRepository
import org.junit.After
import org.junit.Before
import org.junit.Test

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class UpdateAlertImplTest {

    private val mainThreadSurrogate = newSingleThreadContext("main")

    private val repository: AlertsRepository = mockk(relaxed = true)
    private lateinit var updateAlertImpl: UpdateAlertImpl


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        updateAlertImpl = UpdateAlertImpl(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `When update is called with alert object, then value is forwarded to repository`() =
        runBlockingTest {
            // given
            val alert = Alert(id = 324)

            // when
            updateAlertImpl.update(alert)

            // then
            coVerify { repository.updateAlert(alert) }
        }
}
