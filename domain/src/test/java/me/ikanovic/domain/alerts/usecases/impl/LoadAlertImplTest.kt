package me.ikanovic.domain.alerts.usecases.impl

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.AlertsRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
@ObsoleteCoroutinesApi
class LoadAlertImplTest {

    private val mainThreadSurrogate = newSingleThreadContext("main")

    private val repository = mockk<AlertsRepository>(relaxed = true)
    private lateinit var loadAlertImpl: LoadAlertImpl


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)

        loadAlertImpl = LoadAlertImpl(repository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `When load alert with id is called, then the id is forwarded to repository and result returned`() =
        runBlockingTest {
            // given
            val alertId = 32L
            val expectedAlert = Alert(alertId)
            coEvery { repository.loadAlert(alertId) } returns expectedAlert

            // when
            val result = loadAlertImpl.loadAlert(alertId)

            // then
            coVerify { repository.loadAlert(alertId) }
            assertEquals(expectedAlert, result)
        }


}