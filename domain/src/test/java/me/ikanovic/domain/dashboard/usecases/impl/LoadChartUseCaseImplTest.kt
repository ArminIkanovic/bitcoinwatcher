package me.ikanovic.domain.dashboard.usecases.impl

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import me.ikanovic.domain.dashboard.DashboardRepository
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_CAP
import org.junit.Before
import org.junit.Test


class LoadChartUseCaseImplTest {

    private val timeSpan = "timeSpan"

    private val repository = mockk<DashboardRepository>()
    private lateinit var loadChartUseCaseImpl: LoadChartUseCaseImpl


    @Before
    fun setUp() {
        loadChartUseCaseImpl = LoadChartUseCaseImpl(repository)
    }

    @Test
    fun `When load chart with chartId and time span is called, then repository load chart with same params is invoked`() {
        //given
        every { repository.loadChartData(any(), any()) }.returns(Single.never())

        //when
        loadChartUseCaseImpl.load(MARKET_CAP, timeSpan)
            .test()
            .dispose()

        //then
        verify { repository.loadChartData(MARKET_CAP, timeSpan) }
    }

    @Test
    fun `When load chart is called and repository returns result, then same result is returned`() {
        //given
        val chart = mockk<Chart>()
        every { repository.loadChartData(any(), any()) }.returns(Single.just(chart))

        //when
        val result = loadChartUseCaseImpl.load(MARKET_CAP, timeSpan)
            .test()

        //then
        result
            .assertResult(chart)
            .assertNoErrors()
            .assertComplete()
            .dispose()
    }
}
