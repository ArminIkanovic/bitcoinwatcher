package me.ikanovic.domain.dashboard.entities

data class Chart(
    val chartId: String,
    val name: String,
    val unit: String,
    val period: Period,
    val description: String,
    val values: List<Value>
) {

    companion object {
        const val TRADE_VOLUME = "trade-volume"
        const val MARKET_PRICE = "market-price"
        const val MARKET_CAP = "market-cap"

        const val ONE_HOUR_TIME_SPAN = "1hour"
        const val THREE_DAYS_TIME_SPAN = "3days"
    }
}
