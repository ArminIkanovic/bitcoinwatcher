package me.ikanovic.domain.dashboard.entities

import java.util.*

enum class Period {
    Minute,
    Hour,
    Day,
    Week,
    Year;

    companion object {
        fun from(period: String): Period {
            return values().firstOrNull { it.name.toLowerCase(Locale.US) == period }
                ?: Day
        }
    }
}
