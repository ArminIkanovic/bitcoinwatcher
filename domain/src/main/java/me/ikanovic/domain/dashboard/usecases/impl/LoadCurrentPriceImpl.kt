package me.ikanovic.domain.dashboard.usecases.impl

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.withContext
import me.ikanovic.domain.dashboard.DashboardRepository
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.usecases.LoadCurrentPrice
import javax.inject.Inject

class LoadCurrentPriceImpl @Inject constructor(
    private val dashboardRepository: DashboardRepository
) : LoadCurrentPrice {

    override suspend fun currentPrice(): Double {
        return withContext(Dispatchers.IO) {
            val chartData = dashboardRepository.loadChartData(
                Chart.MARKET_PRICE,
                Chart.ONE_HOUR_TIME_SPAN
            ).await()

            return@withContext chartData.values.last().y
        }
    }
}