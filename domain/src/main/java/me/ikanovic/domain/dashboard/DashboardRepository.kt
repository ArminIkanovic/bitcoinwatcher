package me.ikanovic.domain.dashboard

import io.reactivex.Single
import me.ikanovic.domain.dashboard.entities.Chart

interface DashboardRepository {

    fun loadChartData(
        chartId: String,
        timeSpan: String
    ): Single<Chart>

}
