package me.ikanovic.domain.dashboard.usecases

import io.reactivex.Single
import me.ikanovic.domain.dashboard.entities.Chart

interface LoadChartUseCase {
    fun load(chartId: String, timeSpan: String): Single<Chart>
}
