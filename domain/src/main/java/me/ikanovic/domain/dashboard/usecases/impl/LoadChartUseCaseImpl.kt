package me.ikanovic.domain.dashboard.usecases.impl

import io.reactivex.Single
import me.ikanovic.domain.dashboard.DashboardRepository
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.usecases.LoadChartUseCase
import javax.inject.Inject

class LoadChartUseCaseImpl @Inject constructor(
    private val repository: DashboardRepository
) : LoadChartUseCase {

    override fun load(chartId: String, timeSpan: String): Single<Chart> {
        return repository.loadChartData(chartId, timeSpan)
    }

}
