package me.ikanovic.domain.dashboard.usecases

interface LoadCurrentPrice {
    suspend fun currentPrice(): Double
}