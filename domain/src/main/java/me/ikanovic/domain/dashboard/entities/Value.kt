package me.ikanovic.domain.dashboard.entities

data class Value(
    val x: Long,
    val y: Double
)
