package me.ikanovic.domain.notifications.usecase

import me.ikanovic.domain.notifications.entities.AlertNotification

interface DispatchNotification {

    suspend fun dispatchNotification(notification: AlertNotification)

}