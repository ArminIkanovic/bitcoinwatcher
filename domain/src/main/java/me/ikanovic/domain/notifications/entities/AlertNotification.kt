package me.ikanovic.domain.notifications.entities

import me.ikanovic.domain.alerts.entities.Alert

data class AlertNotification(
    val alert: Alert
)