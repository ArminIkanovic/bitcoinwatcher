package me.ikanovic.domain.alerts.usecases.impl

import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.alerts.usecases.UpdateAlert
import javax.inject.Inject

class UpdateAlertImpl @Inject constructor(
    private val repository: AlertsRepository
): UpdateAlert {

    override suspend fun update(alert: Alert) {
        return repository.updateAlert(alert)
    }
}