package me.ikanovic.domain.alerts.usecases

import me.ikanovic.domain.alerts.entities.Alert

interface RemoveAlert {
    suspend fun remove(alert: Alert)
}