package me.ikanovic.domain.alerts.usecases.impl

import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.usecases.TriggerAlert
import me.ikanovic.domain.notifications.entities.AlertNotification
import me.ikanovic.domain.notifications.usecase.DispatchNotification
import javax.inject.Inject

class TriggerAlertImpl @Inject constructor(
    private val alertsRepository: AlertsRepository,
    private val dispatchNotification: DispatchNotification
) : TriggerAlert {
    override suspend fun trigger(alert: Alert) {
        if (!alert.isTriggered) {
            alertsRepository.updateAlert(alert.copy(isTriggered = true))
            sendNotification(alert)
        }
    }

    private suspend fun sendNotification(alert: Alert) {
        dispatchNotification.dispatchNotification(
            AlertNotification(
                alert
            )
        )
    }
}