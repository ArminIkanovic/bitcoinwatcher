package me.ikanovic.domain.alerts.usecases.impl

import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.alerts.usecases.RemoveAlert
import javax.inject.Inject

class RemoveAlertImpl @Inject constructor(
    private val alertsRepository: AlertsRepository
): RemoveAlert {
    override suspend fun remove(alert: Alert) {
        return alertsRepository.removeAlert(alert)
    }
}