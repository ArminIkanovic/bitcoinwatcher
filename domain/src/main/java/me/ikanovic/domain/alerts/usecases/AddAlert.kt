package me.ikanovic.domain.alerts.usecases

import me.ikanovic.domain.alerts.entities.Alert

interface AddAlert {
    suspend fun addAlert(alert: Alert)
}