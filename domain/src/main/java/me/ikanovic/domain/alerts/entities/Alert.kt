package me.ikanovic.domain.alerts.entities


data class Alert(
    val id: Long = 0,
    val value: Double = 0.0,
    val startValue: Double = 0.0,
    val isTriggered: Boolean = false
)