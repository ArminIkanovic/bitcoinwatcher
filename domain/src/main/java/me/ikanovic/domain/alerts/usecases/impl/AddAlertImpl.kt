package me.ikanovic.domain.alerts.usecases.impl

import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.usecases.AddAlert
import me.ikanovic.domain.dashboard.usecases.LoadCurrentPrice
import javax.inject.Inject

class AddAlertImpl @Inject constructor(
    private val alertsRepository: AlertsRepository,
    private val loadCurrentPrice: LoadCurrentPrice
) : AddAlert {

    override suspend fun addAlert(alert: Alert) {
        return alertsRepository.saveAlert(
            alert.copy(
                startValue = loadCurrentPrice.currentPrice()
            )
        )
    }
}