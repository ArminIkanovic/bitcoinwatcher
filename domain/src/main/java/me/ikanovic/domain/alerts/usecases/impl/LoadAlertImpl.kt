package me.ikanovic.domain.alerts.usecases.impl

import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.usecases.LoadAlert
import javax.inject.Inject

class LoadAlertImpl @Inject constructor(
    private val repository: AlertsRepository
) : LoadAlert {

    override suspend fun loadAlerts(): List<Alert> {
        return repository.loadAll()
    }

    override suspend fun loadAlert(id: Long): Alert? {
        return repository.loadAlert(id)
    }
}
