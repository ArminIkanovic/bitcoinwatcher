package me.ikanovic.domain.alerts.usecases

import me.ikanovic.domain.alerts.entities.Alert

interface UpdateAlert {
    suspend  fun update(alert: Alert)
}