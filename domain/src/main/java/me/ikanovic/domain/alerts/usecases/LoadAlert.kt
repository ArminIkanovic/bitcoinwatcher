package me.ikanovic.domain.alerts.usecases

import me.ikanovic.domain.alerts.entities.Alert

interface LoadAlert {

    suspend fun loadAlerts(): List<Alert>

    suspend fun loadAlert(id: Long): Alert?
}