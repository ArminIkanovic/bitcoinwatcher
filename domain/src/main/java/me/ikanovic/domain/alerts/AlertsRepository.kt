package me.ikanovic.domain.alerts

import me.ikanovic.domain.alerts.entities.Alert

interface AlertsRepository {
    suspend fun loadAlert(id: Long): Alert?
    suspend fun loadAll(): List<Alert>
    suspend fun removeAlert(alert: Alert)
    suspend fun updateAlert(alert: Alert)
    suspend fun saveAlert(alert: Alert)
}