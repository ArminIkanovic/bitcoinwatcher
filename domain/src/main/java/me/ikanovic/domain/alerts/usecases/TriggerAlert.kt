package me.ikanovic.domain.alerts.usecases

import me.ikanovic.domain.alerts.entities.Alert

interface TriggerAlert {
    suspend fun trigger(alert: Alert)
}