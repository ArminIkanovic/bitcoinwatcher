# Bitcoin Watcher

## Introduction

This project is tiny Android app used as exploration of Android technologies, libraries and clean architecture. App consists out of two tabs, one with blockchain metrics and another with preparations for price alerts. The metrics shown are loaded via blockchain.info/charts API. The app is 100% Kotlin code.

#### Jetpack Compose
The app has the Settings screen implemented with Jetpack Compose, because of that to build the project Android Studio Canary version is required.

**How to run the app**

1. Open project with Android Studio
1. Run the app

## Architecture

The app code is split into multiple modules in a clean architecture style. The android UI and project setup is located in the 'app' module. The 'domain' with use cases and models. The API interaction and data loading is in the 'data' module. Room database is implemented in the 'room' module.
Interaction with API is implemented using Retrofit and RxJava, whereas data storing into Room database is based on Kotlin coroutines.

The code is covered with unit and instrumented tests.

## Technologies and libs used

### UI/Android

1. Jetpack Compose
1. Jetpack ViewModel 
1. Jetpack LiveData 
1. Jetpack ViewModel Ktx
1. Jetpack Navigation 
1. Jetpack Room
1. Jetpack WorkManager
1. Material Components
1. MPAndroid Charts


### Dependency Injection
1. Dagger Hilt
1. Dagger Hilt for Jetpack


### Concurrency and API    
1. RxJava 2 
1. RxKotlin
1. Retrofit
1. Gson
1. Kotlin coroutines

### Unit and instrumentation tests
1. Mockk
1. Junit
1. AndroidX Testing
1. Espresso
