package me.ikanovic.bitcoinwatcher.ui.dashboard

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.mockk.every
import io.mockk.mockk
import io.mockk.verifyAll
import io.reactivex.Single
import me.ikanovic.bitcoinwatcher.extensions.messageOrDefault
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_CAP
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_PRICE
import me.ikanovic.domain.dashboard.entities.Chart.Companion.TRADE_VOLUME
import me.ikanovic.domain.dashboard.entities.Period
import me.ikanovic.domain.dashboard.entities.Value
import me.ikanovic.domain.dashboard.usecases.LoadChartUseCase
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class DashboardViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    var rxJavaSchedulers: TestRule = testSchedulersTestRule

    private val loadChartUseCase = mockk<LoadChartUseCase>()

    private val emptyObserver = Observer<Any> { }

    @Before
    fun setUp() {
        every { loadChartUseCase.load(MARKET_PRICE, any()) }.returns(
            Single.just(
                createChart(
                    MARKET_PRICE
                )
            )
        )
        every { loadChartUseCase.load(MARKET_CAP, any()) }.returns(
            Single.just(
                createChart(
                    MARKET_CAP
                )
            )
        )
        every { loadChartUseCase.load(TRADE_VOLUME, any()) }.returns(
            Single.just(
                createChart(
                    TRADE_VOLUME
                )
            )
        )
    }

    @Test
    fun `When view model instance is created, then three charts should be loaded`() {
        // given && when
        initViewModel()

        // then
        verifyAll {
            loadChartUseCase.load(MARKET_PRICE, any())
            loadChartUseCase.load(MARKET_CAP, any())
            loadChartUseCase.load(TRADE_VOLUME, any())
        }
    }

    @Test
    fun `When one chart is loaded, then charts contains one chart and progress is updated`() {
        // given

        every { loadChartUseCase.load(MARKET_PRICE, any()) }.returns(Single.never())
        every { loadChartUseCase.load(MARKET_CAP, any()) }.returns(Single.never())

        // when
        val viewModel = initViewModel()
        viewModel.progress.observeForever(emptyObserver)

        // then
        assertEquals(1, viewModel.charts.value?.size)
        assertEquals(100 / 3, viewModel.progress.value)

        // finally
        viewModel.progress.removeObserver(emptyObserver)
    }

    @Test
    fun `When all charts are loaded, then charts live data contains all chart and progress is 100`() {
        // given && when
        val viewModel = initViewModel()
        viewModel.progress.observeForever(emptyObserver)

        // then
        assertEquals(3, viewModel.charts.value?.size)
        assertEquals(100, viewModel.progress.value)

        // finally
        viewModel.progress.removeObserver(emptyObserver)
    }

    @Test
    fun `When use case throws an exception, then error live data is updated with the exception message`() {
        // given
        val exception = Exception("error message")
        every { loadChartUseCase.load(MARKET_PRICE, any()) }.returns(Single.error(exception))

        // when
        val viewModel = initViewModel()

        // then
        assertEquals(exception.messageOrDefault, viewModel.error.value)
    }


    private fun initViewModel() = DashboardViewModel(loadChartUseCase)


    /* Utils */
    private fun createChart(chartId: String): Chart {
        return Chart(
            chartId = chartId,
            name = "chart name",
            unit = "unit",
            period = Period.Day,
            description = "chart description",
            values = listOf(
                Value(0, 0.0),
                Value(0, 1.0)
            )
        )
    }
}
