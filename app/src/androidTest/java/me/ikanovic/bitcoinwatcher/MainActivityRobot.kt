package me.ikanovic.bitcoinwatcher


import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.instanceOf

object MainActivityRobot {

    fun dashboardProgressBar(): ViewInteraction {
        return onView(withId(R.id.progressBar))
    }

    fun isViewWithTextDisplayed(text: String) {
        onView(withText(CoreMatchers.containsString(text)))
            .check(matches(isDisplayed()))
    }
    
    fun alertsButton(): ViewInteraction {
        return onView(withId(R.id.navigation_alerts))
    }

    fun toolbarTitleContainsText(@StringRes stringRes: Int) {
        onView(instanceOf(Toolbar::class.java))
            .check(matches(hasDescendant(withText(stringRes))))
    }
}
