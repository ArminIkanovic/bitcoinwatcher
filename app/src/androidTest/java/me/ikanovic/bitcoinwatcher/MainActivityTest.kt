package me.ikanovic.bitcoinwatcher

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.mockk.*
import io.reactivex.Single
import me.ikanovic.bitcoinwatcher.di.ApiModule
import me.ikanovic.bitcoinwatcher.di.RepositoryModule
import me.ikanovic.data.api.ChartsApi
import me.ikanovic.data.entities.ChartResponse
import me.ikanovic.domain.alerts.AlertsRepository
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_CAP
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_PRICE
import me.ikanovic.domain.dashboard.entities.Chart.Companion.TRADE_VOLUME
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@UninstallModules(ApiModule::class, RepositoryModule::class)
@HiltAndroidTest
class MainActivityTest {


    @BindValue
    @JvmField
    val mockApi = mockk<ChartsApi>()

    @BindValue
    @JvmField
    val alertsRepository: AlertsRepository = mockk(relaxed = true)

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        every { mockApi.loadChart(any(), any()) }.returns(Single.never())
    }

    @Test
    fun When_charts_are_being_loaded_then_progress_bar_is_visible() {
        //give
        val scenario = ActivityScenario.launch(MainActivity::class.java)

        // when
        scenario.moveToState(Lifecycle.State.RESUMED)

        // then
        with(MainActivityRobot) {
            toolbarTitleContainsText(R.string.title_dashboard)
            dashboardProgressBar().check(matches(isDisplayed()))
        }

        verify(atLeast = 3) { mockApi.loadChart(any(), any(), any()) }
    }


    @Test
    fun When_all_charts_are_loaded_then_progress_bar_is_gone() {
        // give
        every { mockApi.loadChart(any(), any()) }.returns(Single.just(ChartResponse("ok")))
        val scenario = ActivityScenario.launch(MainActivity::class.java)

        // when
        scenario.moveToState(Lifecycle.State.RESUMED)

        // then
        with(MainActivityRobot) {
            dashboardProgressBar().check(matches(CoreMatchers.not(isDisplayed())))
        }

        verify(atLeast = 3) { mockApi.loadChart(any(), any(), any()) }
    }

    @Test
    fun When_chart_is_loaded_then_chart_is_shown_in_recycler_view() {
        // given
        every {
            mockApi.loadChart(
                MARKET_PRICE,
                any()
            )
        }.returns(Single.just(validChartResponse.copy(name = MARKET_PRICE)))
        every { mockApi.loadChart(MARKET_CAP, any()) }.returns(
            Single.just(
                validChartResponse.copy(
                    name = MARKET_CAP
                )
            )
        )
        every {
            mockApi.loadChart(
                TRADE_VOLUME,
                any()
            )
        }.returns(Single.just(validChartResponse.copy(name = TRADE_VOLUME)))

        val scenario = ActivityScenario.launch(MainActivity::class.java)

        // when
        scenario.moveToState(Lifecycle.State.RESUMED)

        // then
        onView(withText(MARKET_PRICE)).check(matches(isDisplayed()))
        onView(withText(MARKET_CAP)).check(matches(isDisplayed()))
        onView(withText(TRADE_VOLUME)).check(matches(isDisplayed()))
    }

    @Test
    fun When_chart_loading_throws_an_exception_then_snack_bar_with_error_is_shown() {
        //given
        every { mockApi.loadChart(MARKET_CAP, any()) }.returns(Single.just(errorChartResponse))

        val scenario = ActivityScenario.launch(MainActivity::class.java)

        // when
        scenario.moveToState(Lifecycle.State.RESUMED)

        // then
        with(MainActivityRobot) {
            isViewWithTextDisplayed(errorChartResponse.status)
        }
        verify(atLeast = 3) { mockApi.loadChart(any(), any(), any()) }
    }

    @Test
    fun When_alert_bottom_navigation_is_clicked_then_alerts_fragment_is_shown_and_alerts_are_loaded() {
        // given
        val scenario = ActivityScenario.launch(MainActivity::class.java)
        coEvery { alertsRepository.loadAll() } returns emptyList()
        // when
        with(MainActivityRobot) {
            alertsButton().perform(click())
        }

        // then
        with(MainActivityRobot) {
            toolbarTitleContainsText(R.string.alerts)
        }
        coVerify { alertsRepository.loadAll() }
    }
}