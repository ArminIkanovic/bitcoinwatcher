package me.ikanovic.bitcoinwatcher.alerts.datasource

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import me.ikanovic.bitcoinwatcher.alerts.AlertRoomMapper
import me.ikanovic.bitcoinwatcher.alerts.database.AlertsDao
import me.ikanovic.bitcoinwatcher.alerts.database.AlertsDatabase
import me.ikanovic.domain.alerts.entities.Alert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class AlertsDataSourceImplTest {

    private val mapper = AlertRoomMapper()
    private lateinit var database: AlertsDatabase
    private lateinit var da: AlertsDao
    private lateinit var dataSource: AlertsDataSourceImpl

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(
            context,
            AlertsDatabase::class.java
        ).build()

        da = database.alertsDao()

        dataSource = AlertsDataSourceImpl(
            alertRoomMapper = mapper,
            alertsDao = da
        )
    }


    @After
    @Throws(IOException::class)
    fun tearDown() {
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun When_create_alert_is_called_then_new_alert_is_inserted_and_id_updated_in_return_value() {
        runBlocking {
            // given
            val alert = Alert(value = 4040.0)

            // when
            val result = dataSource.createAlert(alert)

            // then
            val storedObject = dataSource.getAlert(result.id)

            assertEquals(result, storedObject)
        }
    }

    @Test
    @Throws(Exception::class)
    fun When_alert_is_updated_then_new_alert_is_inserted_and_id_updated_in_return_value() {
        runBlocking {
            // given
            val storedAlert = dataSource.createAlert(Alert(value = 4040.0))
            val updatedAlert = storedAlert.copy(value = 3113.0)

            // when
            dataSource.updateAlert(updatedAlert)

            // then
            val result = dataSource.getAlert(storedAlert.id)
            assertEquals(result, updatedAlert)
        }
    }
}