package me.ikanovic.bitcoinwatcher.compose.examples

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.coil.CoilImage
import kotlinx.coroutines.launch
import me.ikanovic.bitcoinwatcher.compose.theme.BitcoinWatcherTheme


@Composable
fun SimpleList() {
    val listSize = 100
    val scrollState = rememberLazyListState()
    val composableScope = rememberCoroutineScope()
    Column {
        Row {
            Button(onClick = {
                composableScope.launch {
                    scrollState.animateScrollToItem(0)
                }
            }) {
                Text(text = "Scroll to top")
            }
            Button(onClick = {
                composableScope.launch {
                    scrollState.animateScrollToItem(listSize - 1)
                }
            }) {
                Text(text = "Scroll to bottom")
            }
        }
        LazyColumn(state = scrollState) {
            items(listSize) {
                ImageListItem(index = it)
            }
        }
    }
}

@Composable
fun ImageListItem(index: Int) {
    Row(verticalAlignment = Alignment.CenterVertically) {
        CoilImage(
            data = "https://developer.android.com/images/brand/Android_Robot.png",
            contentDescription = "Android Logo",
            modifier = Modifier.size(50.dp)
        )
        Spacer(Modifier.width(10.dp))
        Text(text = "item #$index")
    }
}

@Preview(showBackground = true)
@Composable
fun SimpleListPreview() {
    BitcoinWatcherTheme {
        SimpleList()
    }
}