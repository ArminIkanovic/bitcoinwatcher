package me.ikanovic.bitcoinwatcher.compose.theme


const val smallCornerRadius = 4
const val mediumCornerRadius = 4
const val largeCornerRadius = 0
const val defaultPadding = 16
