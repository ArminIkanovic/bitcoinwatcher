package me.ikanovic.bitcoinwatcher.compose.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(smallCornerRadius.dp),
    medium = RoundedCornerShape(mediumCornerRadius.dp),
    large = RoundedCornerShape(largeCornerRadius.dp)
)