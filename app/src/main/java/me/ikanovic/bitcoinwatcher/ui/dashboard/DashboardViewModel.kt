package me.ikanovic.bitcoinwatcher.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import me.ikanovic.bitcoinwatcher.extensions.messageOrDefault
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_CAP
import me.ikanovic.domain.dashboard.entities.Chart.Companion.MARKET_PRICE
import me.ikanovic.domain.dashboard.entities.Chart.Companion.THREE_DAYS_TIME_SPAN
import me.ikanovic.domain.dashboard.entities.Chart.Companion.TRADE_VOLUME
import me.ikanovic.domain.dashboard.usecases.LoadChartUseCase
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    loadChartUseCase: LoadChartUseCase
) : ViewModel() {


    private val chartsToLoad = 3

    private val compositeDisposable = CompositeDisposable()

    private val _charts = MutableLiveData<List<Chart>>(mutableListOf())
    val charts: LiveData<List<Chart>> = _charts

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _progress = MediatorLiveData<Int>().apply {
        addSource(_charts) {
            if (it.size == chartsToLoad) {
                this.value = 100
            } else {
                this.value = (100 / chartsToLoad) * (it.size)
            }
        }
    }
    val progress: LiveData<Int> = _progress

    init {

        loadChartUseCase.load(MARKET_PRICE, THREE_DAYS_TIME_SPAN)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { updateErrorLiveData(it) },
                onSuccess = { updateCharts(it) }
            )
            .addTo(compositeDisposable)

        loadChartUseCase.load(MARKET_CAP, THREE_DAYS_TIME_SPAN)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { updateErrorLiveData(it) },
                onSuccess = { updateCharts(it) }
            )
            .addTo(compositeDisposable)

        loadChartUseCase.load(TRADE_VOLUME, THREE_DAYS_TIME_SPAN)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { updateErrorLiveData(it) },
                onSuccess = { updateCharts(it) }
            )
            .addTo(compositeDisposable)
    }

    private fun updateCharts(it: Chart) {
        _charts.value = _charts.value?.plus(it)
    }

    private fun updateErrorLiveData(it: Throwable) {
        _error.value = it.messageOrDefault
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
