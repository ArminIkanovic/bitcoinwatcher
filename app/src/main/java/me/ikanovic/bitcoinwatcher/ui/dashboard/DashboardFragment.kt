package me.ikanovic.bitcoinwatcher.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import me.ikanovic.bitcoinwatcher.databinding.FragmentDashboardBinding

@AndroidEntryPoint
class DashboardFragment : Fragment() {

    private val adapter by lazy { DashboardAdapter() }
    private val dashboardViewModel: DashboardViewModel by viewModels()

    private var binding: FragmentDashboardBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentDashboardBinding.inflate(layoutInflater, container, false)
        binding?.let { setUpViews(it) }

        return binding?.root
    }

    private fun setUpViews(it: FragmentDashboardBinding) {
        it.dashboardCharts.adapter = adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        dashboardViewModel.charts.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        dashboardViewModel.error.observe(viewLifecycleOwner) { error ->
            binding?.dashboardCharts?.let { view ->
                Snackbar.make(view, error, Snackbar.LENGTH_SHORT).show()
            }
        }

        dashboardViewModel.progress.observe(viewLifecycleOwner) {
            updateProgressBar(it)
        }
    }

    private fun updateProgressBar(it: Int) {
        binding?.progressBar?.progress = it
        binding?.progressBar?.visibility = if (it == 100) GONE else VISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}
