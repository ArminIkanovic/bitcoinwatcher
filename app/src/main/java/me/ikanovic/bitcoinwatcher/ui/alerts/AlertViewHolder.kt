package me.ikanovic.bitcoinwatcher.ui.alerts

import androidx.recyclerview.widget.RecyclerView
import me.ikanovic.bitcoinwatcher.R
import me.ikanovic.bitcoinwatcher.databinding.ViewHolderAlertBinding
import me.ikanovic.domain.alerts.entities.Alert

class AlertViewHolder(
    private val binding: ViewHolderAlertBinding
) : RecyclerView.ViewHolder(binding.root) {

    lateinit var alert: Alert

    fun bind(alert: Alert) {
        this.alert = alert

        binding.value.text = itemView.context.getString(
            R.string.alert_usd_value,
            alert.value
        )
        binding.info.text = itemView.context.getString(
            R.string.alert_hit,
            alert.isTriggered.toYesNo()
        )
    }
}

private fun Boolean.toYesNo(): String = if (this) "Yes" else "No"
