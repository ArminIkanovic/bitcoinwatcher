package me.ikanovic.bitcoinwatcher.ui.settings

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import dagger.hilt.android.AndroidEntryPoint
import me.ikanovic.bitcoinwatcher.compose.theme.BitcoinWatcherTheme
import me.ikanovic.bitcoinwatcher.compose.theme.defaultPadding
import me.ikanovic.bitcoinwatcher.theme.ThemeHelper
import me.ikanovic.bitcoinwatcher.theme.ThemeMode
import javax.inject.Inject

@AndroidEntryPoint
class SettingsActivity : ComponentActivity() {

    @Inject
    lateinit var themeHelper: ThemeHelper

    private lateinit var settingsViewModel: SettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            settingsViewModel = viewModel()
            val isDark = settingsViewModel.themeMode == ThemeMode.DARK

            BitcoinWatcherTheme(darkTheme = isDark) {
                Surface(color = MaterialTheme.colors.surface) {
                    SettingsScreen(
                        themeMode = settingsViewModel.themeMode,
                        onChangeThemeMode = settingsViewModel::changeThemeMode,
                        onNavigationClick = { finish() },
                    )
                }
            }

        }
    }
}

@Composable
fun SettingsScreen(
    modifier: Modifier = Modifier,
    themeMode: ThemeMode,
    onNavigationClick: () -> Unit,
    onChangeThemeMode: (ThemeMode) -> Unit
) {
    Scaffold(
        modifier = modifier,
        topBar = { SettingsTopBar(onNavigationClick) }
    ) { innerPadding ->
        SettingsBody(
            Modifier
                .padding(innerPadding)
                .padding(defaultPadding.dp),
            themeMode,
            onChangeThemeMode
        )
    }
}

@Composable
private fun SettingsTopBar(onNavigationClick: () -> Unit) {
    TopAppBar(
        navigationIcon = {
            IconButton(onClick = onNavigationClick) {
                Icon(Icons.Filled.Close, contentDescription = null)
            }
        },
        title = {
            Text(text = "Settings")
        }
    )
}

@Composable
private fun SettingsBody(
    modifier: Modifier = Modifier,
    themeMode: ThemeMode,
    onChangeThemeMode: (ThemeMode) -> Unit
) {
    Column(
        modifier
            .padding(defaultPadding.dp)
            .fillMaxHeight()
    ) {
        Column(Modifier.weight(1f)) {
            SettingsScreenTitle(text = "Theme style")
            Spacer(modifier = Modifier.height(8.dp))
            ThemeModesRow(
                onChangeThemeMode = onChangeThemeMode,
                themeMode = themeMode
            )
        }
    }
}

@Composable
fun SettingsScreenTitle(
    modifier: Modifier = Modifier,
    text: String
) {
    Text(
        modifier = modifier,
        text = text,
        style = MaterialTheme.typography.h6
    )
}

@Composable
private fun ThemeModesRow(
    modifier: Modifier = Modifier,
    onChangeThemeMode: (ThemeMode) -> Unit,
    themeMode: ThemeMode
) {
    Row(modifier) {
        ThemeMode.values().forEach {
            val onClick = { onChangeThemeMode(it) }
            if (themeMode == it) {
                Button(
                    onClick = onClick
                ) {
                    Text(text = it.name)
                }
            } else {
                OutlinedButton(onClick = onClick) {
                    Text(text = it.name)
                }
            }
            Spacer(modifier = Modifier.width(8.dp))
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    BitcoinWatcherTheme {
        SettingsScreen(
            themeMode = ThemeMode.DARK,
            onChangeThemeMode = {},
            onNavigationClick = {},
        )
    }
}
