package me.ikanovic.bitcoinwatcher.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import me.ikanovic.bitcoinwatcher.R
import me.ikanovic.bitcoinwatcher.databinding.ViewDashboardChartCardBinding
import me.ikanovic.domain.dashboard.entities.Chart

class DashboardAdapter(
    diffCallback: DiffUtil.ItemCallback<Chart> = DiffCallback()
) : ListAdapter<Chart, DashboardViewHolder>(diffCallback) {

    override fun getItemViewType(position: Int): Int = R.layout.view_dashboard_chart_card

    override fun onBindViewHolder(holder: DashboardViewHolder, position: Int) {
        holder.bindChart(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardViewHolder {
        return DashboardViewHolder(
            ViewDashboardChartCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    class DiffCallback : DiffUtil.ItemCallback<Chart>() {
        override fun areContentsTheSame(oldItem: Chart, newItem: Chart): Boolean =
            oldItem == newItem

        override fun areItemsTheSame(oldItem: Chart, newItem: Chart): Boolean =
            oldItem.chartId == newItem.chartId
    }
}
