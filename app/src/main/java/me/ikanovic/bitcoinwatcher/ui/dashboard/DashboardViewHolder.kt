package me.ikanovic.bitcoinwatcher.ui.dashboard

import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import me.ikanovic.bitcoinwatcher.R
import me.ikanovic.bitcoinwatcher.databinding.ViewDashboardChartCardBinding
import me.ikanovic.bitcoinwatcher.extensions.getColorFromAttr
import me.ikanovic.domain.dashboard.entities.Chart
import me.ikanovic.domain.dashboard.entities.Value


class DashboardViewHolder(
    private val binding: ViewDashboardChartCardBinding
) : RecyclerView.ViewHolder(binding.root) {

    private val onSurfaceColor get() = itemView.context.getColorFromAttr(R.attr.colorOnSurface)
    private val primaryColor get() = itemView.context.getColorFromAttr(R.attr.colorPrimary)

    fun bindChart(item: Chart?) {
        item ?: return
        binding.title.text = item.name
        binding.description.text = item.description

        updateChartData(item)
    }

    private fun updateChartData(item: Chart) {
        binding.lineChart.description.isEnabled = false
        binding.lineChart.legend.isEnabled = false

        binding.lineChart.axisLeft.isEnabled = false

        binding.lineChart.axisRight.textColor = onSurfaceColor
        binding.lineChart.axisRight.valueFormatter = getValueFormatter(item.values.firstOrNull()?.y?: 0.0)

        binding.lineChart.xAxis.valueFormatter = DateValueFormatter()
        binding.lineChart.xAxis.textColor = onSurfaceColor

        val dataSet = LineDataSet(
            item.values.map { it.toDataEntry() },
            item.name
        )

        dataSet.setCircleColor(primaryColor)
        dataSet.color = primaryColor
        dataSet.valueTextColor = onSurfaceColor
        dataSet.valueFormatter = getValueFormatter(item.values.firstOrNull()?.y?: 0.0)

        binding.lineChart.data = LineData(dataSet)
    }

    private fun getValueFormatter(y: Double): ValueFormatter {
        return if (y > 1_000_000) {
            LargeValueFormatter()
        } else {
            DefaultAxisValueFormatter(2)
        }
    }
}

fun Value.toDataEntry() = Entry(this.x.toFloat(), this.y.toFloat())
