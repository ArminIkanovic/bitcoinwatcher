package me.ikanovic.bitcoinwatcher.ui.dashboard

import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.SimpleDateFormat
import java.util.*

class DateValueFormatter : ValueFormatter() {

    private val mFormat: SimpleDateFormat = SimpleDateFormat("dd MMM", Locale.ENGLISH)

    override fun getFormattedValue(value: Float): String {
        return mFormat.format(Date(value.toLong() * 1000L))
    }
}
