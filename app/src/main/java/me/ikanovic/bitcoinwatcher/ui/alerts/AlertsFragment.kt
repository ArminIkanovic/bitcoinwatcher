package me.ikanovic.bitcoinwatcher.ui.alerts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import me.ikanovic.bitcoinwatcher.R
import me.ikanovic.bitcoinwatcher.databinding.AlertsFragmentBinding
import me.ikanovic.bitcoinwatcher.extensions.getColorFromAttr
import me.ikanovic.bitcoinwatcher.extensions.hideSoftInput
import me.ikanovic.bitcoinwatcher.extensions.showSoftInput
import me.ikanovic.domain.alerts.entities.Alert

@AndroidEntryPoint
class AlertsFragment : Fragment() {

    private val adapter = AlertsAdapter()
    private val viewModel: AlertsViewModel by viewModels()
    private var binding: AlertsFragmentBinding? = null

    private val onBackPressedCallback by lazy {
        object : OnBackPressedCallback(false) {
            override fun handleOnBackPressed() {
                if (viewModel.showAddNew.value == true) {
                    viewModel.toggleAddNew()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AlertsFragmentBinding.inflate(inflater, container, false)
        binding?.let { setUpUi(it) }
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(onBackPressedCallback)
        setUpVm()
    }

    private fun setUpVm() {
        binding?.let {
            viewModel.alerts.observe(viewLifecycleOwner) {
                adapter.submitList(it)
            }

            viewModel.deletedAlert.observe(viewLifecycleOwner) {
                handleShowSnackBar(it)
            }

            viewModel.showAddNew.observe(viewLifecycleOwner) {
                binding?.run { handleShowAddNewAlert(it) }
            }

            viewModel.errorMessage.observe(viewLifecycleOwner) {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun handleShowSnackBar(alert: Alert?) {
        alert?.let { showDeleteSnackbar(it) }
    }

    private fun AlertsFragmentBinding.handleShowAddNewAlert(it: Boolean) {
        addNewContainer.isVisible = it
        addNewBackground.isVisible = it
        showAddNew.isVisible = !it
        onBackPressedCallback.isEnabled = it
        if (it) {
            addNewValueEditText.text?.clear()
            addNewValueEditText.requestFocus()
            requireActivity().showSoftInput()
        } else {
            addNewValueEditText.clearFocus()
            requireActivity().hideSoftInput()
        }
    }

    private fun showDeleteSnackbar(alert: Alert) {
        Snackbar.make(requireView(), R.string.undo_alert_delete, Snackbar.LENGTH_LONG)
            .setBackgroundTint(requireContext().getColorFromAttr(R.attr.colorError))
            .setAction(R.string.undo) {
                viewModel.undoAlertDelete(alert)
            }
            .addCallback(
                object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        if (event == DISMISS_EVENT_TIMEOUT || event == DISMISS_EVENT_CONSECUTIVE) {
                            viewModel.confirmDeleteAlert()
                        }
                    }
                }
            )
            .show()
    }

    private fun setUpUi(binding: AlertsFragmentBinding) {

        binding.alertsRecyclerView.adapter = adapter

        ItemTouchHelper(AlertsCallbackListener(viewModel::alertDeleted)).attachToRecyclerView(
            binding.alertsRecyclerView
        )

        binding.addNewAlertButton.setOnClickListener {
            viewModel.addAlert(binding.addNewValueEditText.text.toString())
        }

        binding.showAddNew.setOnClickListener {
            viewModel.toggleAddNew()
        }
        binding.addNewBackground.setOnClickListener {
            viewModel.toggleAddNew()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        onBackPressedCallback.remove()
        binding = null
    }
}
