package me.ikanovic.bitcoinwatcher.ui.alerts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import me.ikanovic.bitcoinwatcher.extensions.messageOrDefault
import me.ikanovic.domain.alerts.entities.Alert
import me.ikanovic.domain.alerts.usecases.AddAlert
import me.ikanovic.domain.alerts.usecases.LoadAlert
import me.ikanovic.domain.alerts.usecases.RemoveAlert
import java.util.*
import javax.inject.Inject

@HiltViewModel
class AlertsViewModel @Inject constructor(
    private val loadAlert: LoadAlert,
    private val addAlert: AddAlert,
    private val removeAlert: RemoveAlert
) : ViewModel() {

    private val _alerts = MutableLiveData<List<Alert>>()
    val alerts: LiveData<List<Alert>> = _alerts

    private val _showAddNew = MutableLiveData<Boolean>()
    val showAddNew: LiveData<Boolean> = _showAddNew

    private val _deletedAlert = MutableLiveData<Alert?>()
    val deletedAlert: LiveData<Alert?> = _deletedAlert

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    init {
        loadAllAlerts()
    }

    private fun loadAllAlerts() {
        viewModelScope.launch {
            _alerts.postValue(loadAlert.loadAlerts())
        }
    }

    fun toggleAddNew() {
        _showAddNew.value = !(_showAddNew.value ?: false)
    }


    fun addAlert(value: String?) {
        viewModelScope.launch {
            try {
                val doubleValue = value?.toDoubleOrNull()
                doubleValue ?: return@launch
                addAlert.addAlert(
                    Alert(
                        value = doubleValue,
                        isTriggered = false,
                    )
                )
                loadAllAlerts()
                toggleAddNew()
            } catch (e: Exception) {
                _errorMessage.postValue(e.messageOrDefault)
            }
        }
    }


    fun alertDeleted(alert: Alert) {
        _alerts.value = alerts.value?.minus(alert)
        _deletedAlert.value = alert
    }

    fun undoAlertDelete(alert: Alert) {
        if (_deletedAlert.value == alert) {
            loadAllAlerts()
            _deletedAlert.value = null
        }
    }

    fun confirmDeleteAlert() {
        _deletedAlert.value?.let {
            viewModelScope.launch {
                removeAlert.remove(it)
                loadAllAlerts()
            }
        }
    }
}
