package me.ikanovic.bitcoinwatcher.ui.settings

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import me.ikanovic.bitcoinwatcher.theme.ThemeHelper
import me.ikanovic.bitcoinwatcher.theme.ThemeMode
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val themeHelper: ThemeHelper
) : ViewModel() {

    var themeMode: ThemeMode by mutableStateOf(themeHelper.currentMode)
        private set


    fun changeThemeMode(newMode: ThemeMode) {
        themeHelper.applyTheme(newMode)
        themeMode = themeHelper.currentMode
    }
}
