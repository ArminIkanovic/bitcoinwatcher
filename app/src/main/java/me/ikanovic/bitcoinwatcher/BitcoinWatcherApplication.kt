package me.ikanovic.bitcoinwatcher

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import me.ikanovic.bitcoinwatcher.theme.ThemeHelper
import me.ikanovic.bitcoinwatcher.workmanager.WorkScheduler
import javax.inject.Inject

@HiltAndroidApp
class BitcoinWatcherApplication : Application(), Configuration.Provider {

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    @Inject
    lateinit var themeHelper: ThemeHelper

    @Inject
    lateinit var workScheduler: WorkScheduler

    override fun onCreate() {
        super.onCreate()
        themeHelper.applyTheme()
        workScheduler.scheduleAlertsChecker()
    }

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

}
