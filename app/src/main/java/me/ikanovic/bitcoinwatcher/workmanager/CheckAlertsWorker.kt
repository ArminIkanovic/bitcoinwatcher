package me.ikanovic.bitcoinwatcher.workmanager

import android.content.Context
import android.util.Log
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import me.ikanovic.domain.alerts.usecases.LoadAlert
import me.ikanovic.domain.alerts.usecases.TriggerAlert
import me.ikanovic.domain.dashboard.usecases.LoadCurrentPrice

@HiltWorker
class CheckAlertsWorker @AssistedInject constructor(
    @Assisted private val appContext: Context,
    @Assisted workerParameters: WorkerParameters,
    private val loadCurrentPrice: LoadCurrentPrice,
    private val loadAlert: LoadAlert,
    private val triggerAlert: TriggerAlert
) : CoroutineWorker(appContext, workerParameters) {

    override suspend fun doWork(): Result {
        Log.d("CheckAlertsWorker", "doWork")
        try {
            val currentPrice = loadCurrentPrice.currentPrice()

            loadAlert.loadAlerts()
                .filter { it.isTriggered.not() }
                .forEach {
                    if (isStopped) {
                        Log.d("CheckAlertsWorker", "doWork() stopped")
                        return Result.success()
                    }
                    if (it.startValue <= it.value) {
                        if (currentPrice >= it.value) {
                            triggerAlert.trigger(it)
                        }
                    } else {
                        if (currentPrice <= it.value) {
                            triggerAlert.trigger(it)
                        }
                    }
                }
            Log.d("CheckAlertsWorker", "doWork() success")
            return Result.success()
        } catch (e: Exception) {
            Log.d("CheckAlertsWorker", "doWork() failure")
            return Result.failure()
        }
    }

    companion object {
        const val workName = "CheckAlertsWorker"
    }
}