package me.ikanovic.bitcoinwatcher.workmanager

import android.content.Context
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import dagger.hilt.android.qualifiers.ApplicationContext
import java.time.Duration
import javax.inject.Inject

class WorkScheduler @Inject constructor(
    @ApplicationContext private val appContext: Context
) {

    fun scheduleAlertsChecker() {
        val alertsWorkRequest = PeriodicWorkRequestBuilder<CheckAlertsWorker>(
            Duration.ofMinutes(15)
        ).build()

        WorkManager.getInstance(appContext)
            .enqueueUniquePeriodicWork(
                CheckAlertsWorker.workName,
                ExistingPeriodicWorkPolicy.REPLACE,
                alertsWorkRequest
            )
    }
}