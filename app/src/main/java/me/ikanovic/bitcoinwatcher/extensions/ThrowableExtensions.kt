package me.ikanovic.bitcoinwatcher.extensions

val Throwable.messageOrDefault: String
    get() = this.message ?: "Oops, an error happened! Please try again"