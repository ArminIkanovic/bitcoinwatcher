package me.ikanovic.bitcoinwatcher.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager.IMPORTANCE_DEFAULT
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import dagger.hilt.android.qualifiers.ApplicationContext
import me.ikanovic.bitcoinwatcher.R
import javax.inject.Inject

class NotificationSender @Inject constructor(
    @ApplicationContext private val context: Context
) {

    fun sendNotification(id: Int, title: String, message: String?) {

        setUpChannel()

        NotificationManagerCompat
            .from(context)
            .notify(id, buildNotification(title, message))

    }

    private fun setUpChannel() {
        NotificationManagerCompat.from(context).createNotificationChannel(
            NotificationChannel(
                ALERTS_CHANNEL_ID,
                "Alerts",
                IMPORTANCE_DEFAULT
            )
        )
    }

    private fun buildNotification(title: String, message: String?): Notification {
        return NotificationCompat.Builder(context, ALERTS_CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .build()
    }

    companion object {
        private const val ALERTS_CHANNEL_ID = "alerts"
    }
}