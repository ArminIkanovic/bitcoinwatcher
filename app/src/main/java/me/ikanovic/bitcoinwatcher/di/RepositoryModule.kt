package me.ikanovic.bitcoinwatcher.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import me.ikanovic.data.repositories.AlertsRepositoryImpl
import me.ikanovic.domain.alerts.AlertsRepository

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun alertsRepository(alertsRepositoryImpl: AlertsRepositoryImpl): AlertsRepository
}