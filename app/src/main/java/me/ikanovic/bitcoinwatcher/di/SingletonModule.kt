package me.ikanovic.bitcoinwatcher.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import me.ikanovic.bitcoinwatcher.alerts.database.AlertsDatabase
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object SingletonModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) = AlertsDatabase.getInstance(context)

    @Provides
    fun provideAlertsDataSource(alertsDatabase: AlertsDatabase) = alertsDatabase.alertsDao()
}
