package me.ikanovic.bitcoinwatcher.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import me.ikanovic.bitcoinwatcher.alerts.datasource.AlertsDataSourceImpl
import me.ikanovic.data.datasources.AlertsDataSource
import me.ikanovic.data.datasources.ChartsDataSource
import me.ikanovic.data.datasources.RemoteChartsDataSourceImpl
import me.ikanovic.data.repositories.DashboardRepositoryImpl
import me.ikanovic.domain.dashboard.DashboardRepository

@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    abstract fun bindChartsDataSource(remoteChartsDataSourceImpl: RemoteChartsDataSourceImpl): ChartsDataSource

    @Binds
    abstract fun bindDashboardRepository(dashboardRepositoryImpl: DashboardRepositoryImpl): DashboardRepository

    @Binds
    abstract fun bindAlertsDataSource(alertsDataSourceImpl: AlertsDataSourceImpl): AlertsDataSource

}
