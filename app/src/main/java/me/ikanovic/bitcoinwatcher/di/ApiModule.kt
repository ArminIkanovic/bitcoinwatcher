package me.ikanovic.bitcoinwatcher.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.ikanovic.bitcoinwatcher.BuildConfig
import me.ikanovic.data.api.ChartsApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Singleton
    @Provides
    fun provideChartApi() = ChartsApi.build(BuildConfig.CHARTS_API_URL)
}