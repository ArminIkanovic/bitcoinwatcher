package me.ikanovic.bitcoinwatcher.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.ikanovic.domain.alerts.usecases.AddAlert
import me.ikanovic.domain.alerts.usecases.LoadAlert
import me.ikanovic.domain.alerts.usecases.RemoveAlert
import me.ikanovic.domain.alerts.usecases.TriggerAlert
import me.ikanovic.domain.alerts.usecases.impl.*
import me.ikanovic.domain.dashboard.usecases.LoadChartUseCase
import me.ikanovic.domain.dashboard.usecases.LoadCurrentPrice
import me.ikanovic.domain.dashboard.usecases.impl.LoadChartUseCaseImpl
import me.ikanovic.domain.dashboard.usecases.impl.LoadCurrentPriceImpl
import me.ikanovic.domain.notifications.usecase.DispatchNotification
import me.ikanovic.domain.notifications.usecase.DispatchNotificationImpl

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCaseModule {

    @Binds
    abstract fun loadChartUseCase(loadChartUseCaseImpl: LoadChartUseCaseImpl): LoadChartUseCase

    @Binds
    abstract fun addAlertUseCase(addAlertImpl: AddAlertImpl): AddAlert

    @Binds
    abstract fun removeAlertUseCase(removeAlertImpl: RemoveAlertImpl): RemoveAlert

    @Binds
    abstract fun updateAlertUseCase(updateAlertImpl: UpdateAlertImpl): UpdateAlertImpl

    @Binds
    abstract fun triggerAlertUseCase(triggerAlertImpl: TriggerAlertImpl): TriggerAlert

    @Binds
    abstract fun loadAlertUseCase(loadAlertImpl: LoadAlertImpl): LoadAlert

    @Binds
    abstract fun dispatchNotificationUseCae(dispatchNotificationImpl: DispatchNotificationImpl): DispatchNotification

    @Binds
    abstract fun loadCurrentPriceUseCase(loadCurrentPrice: LoadCurrentPriceImpl): LoadCurrentPrice
}
