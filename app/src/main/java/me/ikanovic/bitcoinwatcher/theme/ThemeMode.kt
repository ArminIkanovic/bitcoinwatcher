package me.ikanovic.bitcoinwatcher.theme

enum class ThemeMode {
    LIGHT,
    DARK,
    SYSTEM
}