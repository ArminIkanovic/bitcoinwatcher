package me.ikanovic.bitcoinwatcher.theme

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class ThemeHelper @Inject constructor(
    @ApplicationContext private val context: Context
) {
    private val THEME_KEY = "theme"

    private val sharedPref = context.getSharedPreferences(THEME_KEY, Context.MODE_PRIVATE)

    var currentMode: ThemeMode
        get() {
            return ThemeMode.values()[sharedPref.getInt(THEME_KEY, ThemeMode.DARK.ordinal)]
        }
        private set(value) {
            sharedPref.edit()
                .putInt(THEME_KEY, value.ordinal)
                .apply()
        }
    
    fun applyTheme(newMode: ThemeMode = currentMode) {
        currentMode = newMode

        when (newMode) {
            ThemeMode.LIGHT -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            ThemeMode.DARK -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            ThemeMode.SYSTEM -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY)
        }

    }

}