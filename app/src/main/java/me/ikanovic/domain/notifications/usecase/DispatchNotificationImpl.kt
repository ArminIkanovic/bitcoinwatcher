package me.ikanovic.domain.notifications.usecase

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import me.ikanovic.bitcoinwatcher.R
import me.ikanovic.bitcoinwatcher.notifications.NotificationSender
import me.ikanovic.domain.notifications.entities.AlertNotification
import javax.inject.Inject

class DispatchNotificationImpl @Inject constructor(
    @ApplicationContext private val appContext: Context,
    private val notificationSender: NotificationSender
) : DispatchNotification {

    override suspend fun dispatchNotification(notification: AlertNotification) {
        notificationSender.sendNotification(
            notification.alert.id.toInt(),
            appContext.getString(R.string.notification_title, notification.alert.value),
            appContext.getString(R.string.notification_content, notification.alert.value),
        )
    }
}